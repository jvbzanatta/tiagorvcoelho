## 1. Avaliação do escopo

Durante a avaliação do escopo surgiram algumas duvidas as quais foram todas sanadas vi e-mail. A lista de atividades foi modificada para que as atividades possuíssem um escopo menor, e que ficasse mais claro oque estava trabalhando. 
A lista de atividades também está no repositório do projeto. 
As duvidas e questionamento em relação ao escopo foram

1. Existe um requisito que diz que *Lista tem um ou mais itens;* e foi pedido para se criar um endpoint /api/v{n}/list em que o request é um objeto no formato { "title": STRING } . Me pareceu que estava faltando enviar também o item pois se não passar a lista não teria nenhum item e iria contra o requisito. Após uma troca de e-mails o cliente falou que o request poderia ser sem o item mesmo. Para tentar manter o mais simples possível segui fazendo com que o request fosse sem o item, porém eu sugeri mudar o requisito para Lista **pode ter** um ou mais itens;
2. Existe um requisito que diz que *Item pode estar associado a um outro item, assumindo o status de "item filho";* e foi pedido para se criar um endpoint /api/v{n}/list/{ID}/item em que o request é um objeto no formato { "title": STRING, "description": STRING }. Porém este objeto não contém nenhum campo relacionando o item a um outro item filho como no requisito, após esclarecer esta duvida o cliente aceitou em adicionar mais um campo ao request chamado childItem possibilitando assim um item estar associado a outro.

---

## 2. Estimativa em horas do desenvolvimento de TODO o projeto/atividades descritas no escopo

As horas gastas em cada atividade está na planilha que está no repositório, e as atividades forma organizadas da seguinte maneira

1. Criação do projeto inicial (Estimativa: 1 hora)

	O projeto irá utilizar as seguintes tecnologias: maven, spring data, spring security, java 8, spring web, spring aop, h2, swagger, jwt e springboot. Esta atividade consistiu em criar o projeto com todas as dependências e fazer testes rápidos para se ter a certeza que todas as dependências forma importadas corretamente

2. Criação do modelo do banco de dados (Estimativa: 2 horas) 

	Para o banco de dados se decidiu utilizar um banco de dados em memória pela questão de facilidade pois assim não é necessário se instalar um banco de dados, porém o preço a se pagar é que os dados são perdidos ao se reiniciar a aplicação. Foi utilizado spring data, logo a mudança para um banco de dados persistente não necessita de uma mudança no código apenas a mudança em arquivo de configuração. As tabelas e os dados iniciais são todas criadas de forma transparente ao se iniciar a aplicação. Para se executar scripts sql pode se utilizar o console do h2 que pode ser aberto no próprio browser.

3. Configuração do Swagger (Estimativa: 1 hora)

	Para se testar os endpoints existem várias ferramentas como o postman por exemplo, porém foi escolhido utilizar swagger pois ele essa tecnologia cria uma pagina com todos os endpoints da aplicação automaticamente e facilita o teste da aplicação. 

4. Codificação e teste segurança (Estimativa: 8 horas) 

	Existem alguns endpoints os quais necessitam alguma forma de autenticação para que apenas pessoas autorizadas possam utilizar os mesmos. Para essa questão e segurança foi escolhido utilizar spring security juntamente com JWT. JWT é um token criptografado e que possui várias informações, como nome de usuário por exemplo. Escolheu se passar este token no header dos serviços que requerem autorização de forma que se possa testar os endpoints com qualquer ferramenta que possibilite adicionar headers customizados. O token na aplicação possui apenas a informação do usuário e nunca expiram. Foi escolhido colocar apenas o nome e nãos e preocupar com expiração para manter a aplicação o mais simples possível. Para se obter este token foi criado um serviço onde se passa um usuário e senha que está no banco de dados e se retorna o token. Para os testes foram escritos testes unitários utilizando spring, além de se executar testes diretamente na aplicação rodando.

5. Codificação e teste dos endpoints /api/v{n}/list/{ID} e /api/v{n}/list (Estimativa: 8 horas)

	Estes endpoints são responsáveis pela criação de uma lista e de se visualizar a lista. Para a criação destes endpoints juntamente com os serviços que atualizam o banco de dados foi utilizado spring web e spring data. Para os testes foram escritos testes unitários utilizando spring, além de se executar testes diretamente na aplicação rodando.

6. Codificação e teste dos endpoints /api/v{n}/list/{ID}/item /api/v{n}/list/{ID}/item e /api/v{n}/list/{ID}/item/{ID} (Estimativa: 8 horas)

	Estes endpoints são responsáveis pela criação dos itens de uma lista e de se visualizar itens de lista. Para a criação destes endpoints juntamente com os serviços que atualizam o banco de dados foi utilizado spring web e spring data. Para os testes foram escritos testes unitários utilizando spring, além de se executar testes diretamente na aplicação rodando.

---

## 3. Estimativa em DIAS do prazo de entrega

Este projeto foi iniciado dia 4/2/2019 e foi estimado para ser entregue em 3 dias e meio, assumindo que um dia tenha 8 horas, logo a data de entregue deveria ser dia 7/2/2019. Porém como já possuo um trabalho, eu não consigo trabalhar todo dia 8 horas. Trabalho neste projeto apenas a noite ou durante o dia caso não tenha atividades no meu trabalho fixo. Levando em consideração essa questão de já ter um trabalho a estimativa de entrega fica para 11/2/2019.

---

## 4. Aponte as horas e atividade que utilizou para fazer o teste

As atividades já forma mencionadas e brevemente explanadas acima. As horas utilizadas para cada atividade foram

1. Criação do projeto inicial – 1 hora
2. Criação do modelo do banco de dados – 1 hora 
3. Configuração do Swagger – 1 hora
4. Codificação e teste segurança – 6 horas e 15 minutos
5. Codificação e teste dos endpoints /api/v{n}/list/{ID} e /api/v{n}/list – 5 horas
6. Codificação e teste dos endpoints /api/v{n}/list/{ID}/item /api/v{n}/list/{ID}/item e /api/v{n}/list/{ID}/item/{ID} – 8 horas e 30 minutos

---

## 5. Faça as atividades listadas e apontadas como "MUST HAVE" ao final deste documento, desenvolvendo na tecnologia que você definiu. Seguem alguns pontos importantes para a entrega.

a. Tecnologia

	As tecnologias usadas foram maven, spring data, spring security, java 8, spring web, spring aop, h2, swagger, jwt e springboot.

b. Dados simulados

	Os dados são armazenados em um banco de dados em memória, e a carga inicial como usuário por exemplo é inserida automaticamente. Logo não é necessário se preocupar com os dados.

c. Entrega funcional

	Para se testar a aplicação localmente siga os seguintes passos:
		i. Instalar o maven
		ii. Instalar o java 8 ou superior
		ii. Baixar ou clonar este repositório
		iii. Aperte no símbolo do Windows no seu teclado, digite cmd e aperte enter
		iv. Digite cd <Caminho_completo_ate_o_repositorio>
		v. Digite mvn clean install
		vi. Digite cd target
		vii. Digite java -jar ListAndItens-0.0.1-SNAPSHOT.jar
		viii. Em um navegador qualquer digite  http://localhost:8080/api/v1/swagger-ui.html
		ix. Deverá aparecer a pagina contendo os endpoints e onde se pode fazer os testes

	Caso não desejo testar localmente existe o seguinte vídeo comigo testando a versão entregue. O link é https://youtu.be/T6wHltvCgr0
