package vibbra.com.listanditens.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vibbra.com.listanditens.entity.Item;
import vibbra.com.listanditens.repository.ItemRepository;
import vibbra.com.listanditens.service.exception.item.ItemNotFoundException;
import vibbra.com.listanditens.service.exception.item.ItemSaveException;

@Service
public class ItemService {

	@Autowired
	private ItemRepository repository;

	public Item getItem(Long id) {
		return repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id)); 
	}

	public Item getItemByTitleAndDescription(String title, String description) {
		return repository.findByTitleAndDescription(title, description);
	}

	public Item saveItem(Item item) {
		Item itemSaved = repository.save(item);
		if(itemSaved == null) {
			throw new ItemSaveException(item);
		}
		return itemSaved;
	}

	public Item getItemByTitleAndDescriptionAndListId(String title, String description, Long listId) {
		return repository.findByTitleAndDescriptionAndListOwnerId(title, description, listId);
	}

	public Item getItemByItemIdAndListId(Long itemId, Long listId) {
		return repository.findByIdAndListOwnerId(itemId, listId);
	}

}
