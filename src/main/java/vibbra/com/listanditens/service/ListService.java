package vibbra.com.listanditens.service;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vibbra.com.listanditens.controller.list.request.SaveItemToListRequest;
import vibbra.com.listanditens.controller.list.request.SaveListRequest;
import vibbra.com.listanditens.controller.list.request.UpdateItemFromListRequest;
import vibbra.com.listanditens.entity.Item;
import vibbra.com.listanditens.entity.List;
import vibbra.com.listanditens.repository.ListRepository;
import vibbra.com.listanditens.service.exception.item.ItemAlreadyAssociatedWithAnotherListException;
import vibbra.com.listanditens.service.exception.list.ListNotContainItemException;
import vibbra.com.listanditens.service.exception.list.ListNotFoundException;
import vibbra.com.listanditens.service.exception.list.ListSaveException;
import vibbra.com.listanditens.service.exception.list.ListTitleAlreadyUsedException;

@Service
public class ListService {

	@Autowired
	private ListRepository repository;
	
	@Autowired
	private ItemService itemService;
	
	public List getList(Long id) {
		return repository.findById(id).orElseThrow(() -> new ListNotFoundException(id));
	}

	public List saveList(SaveListRequest request) {
		String title = request.getTitle();
		List list = repository.findByTitle(title);
		if(list != null) {
			throw new ListTitleAlreadyUsedException(title);
		} else {
			list = new List();
			list.setTitle(title);
			list.setItens(new HashSet<Item>());
		}
		return saveList(list);
	}

	private List saveList(List list) {
		List listSaved = repository.save(list);
		if(listSaved == null) {
			throw new ListSaveException(list);
		}
		return listSaved;
	}

	public Set<Item> getItensFromList(Long id) {
		return getList(id).getItens();
	}

	public Item saveItemToList(Long id, SaveItemToListRequest request) {
		List list = getList(id);
		String title = request.getTitle();
		String description = request.getDescription();
		//If someone try to save the same item to the same list again we simply return the item
		Item item = itemService.getItemByTitleAndDescriptionAndListId(title, description,id);
		if(item != null) {
			return item;
		} else {
			// One item can be linked to one list only, so if there is another item with same title and description
			// and is not linked to list with id on parameter we should throw an exception
			verifiIfItemAlreadyExist(title , description, id);
			Item newItem = new Item();
			newItem.setTitle(title);
			newItem.setDescription(description);
			newItem.setListOwner(list);
			Long parentItemId = request.getParentItem();
			if(parentItemId != null) {
				Item parentItem = itemService.getItem(parentItemId);
				parentItem.setChildItem(newItem);
				// We need to add the parent item to the list because if we do not we end having the
				// same item as a list item and also a child item of parent item
				list.getItens().add(parentItem);
			} else {
				list.getItens().add(newItem);
			}
			return itemService.saveItem(newItem);
		}
	}

	private void verifiIfItemAlreadyExist(String title, String description, Long id) {
		Item item = itemService.getItemByTitleAndDescription(title, description); 
		if(item != null) {
			List listOwner = item.getListOwner();
			if(listOwner.getId() != id) {
				throw new ItemAlreadyAssociatedWithAnotherListException(item, listOwner);
			}
		} 
	}

	public Item updateItemFromList(Long id, Long itemId, @Valid UpdateItemFromListRequest request) {
		List list = getList(id);
		String title = request.getTitle();
		String description = request.getDescription();
		// One item can be linked to one list only, so if there is another item with same title and description
		// we should throw an exception because if we don't an item will be linked to two lists
		verifiIfItemAlreadyExist(title , description, list.getId());
		Item item = itemService.getItemByItemIdAndListId(itemId, id);
		if(item == null) {
			throw new ListNotContainItemException(itemId);
		}
		item.setTitle(request.getTitle());
		item.setDescription(request.getDescription());
		Long parentItemId = request.getParentItem();
		Boolean removeChild = request.getRemoveChild();
		if(parentItemId != null) {
			Item parentItem = itemService.getItem(parentItemId);
			parentItem.setChildItem(item);
			// We need to remove the item from the list because if we do not we end having the
			// same item as a list item and also a child item of parent item
			list.getItens().remove(item);
		} else if(removeChild != null && removeChild.booleanValue()) {
			item.setChildItem(null);
		}
		return itemService.saveItem(item);
	}
	
}
