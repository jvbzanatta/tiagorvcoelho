package vibbra.com.listanditens.service;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import vibbra.com.listanditens.controller.login.LoginRequest;
import vibbra.com.listanditens.entity.User;
import vibbra.com.listanditens.repository.UserRepository;
import vibbra.com.listanditens.service.exception.user.UserNameNotFoundException;
import vibbra.com.listanditens.service.exception.user.UserWrongPasswordException;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private SecretKey secretKey;
	
	public String getJwtToken(LoginRequest request){
		String username = request.getUsername();
		User user = repository.findByName(username);
		if(user == null) {
			throw new UserNameNotFoundException(username);
		} else {
			if(!user.getPassword().equals(request.getPassword())) {
				throw new UserWrongPasswordException();
			} else {
				return Jwts.builder().setSubject(username).signWith(secretKey).compact();
			}
		}
	}

	public User findUserByName(String name) {
		return repository.findByName(name);
	}

}
