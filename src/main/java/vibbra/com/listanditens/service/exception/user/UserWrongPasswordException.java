package vibbra.com.listanditens.service.exception.user;

@SuppressWarnings("serial")
public class UserWrongPasswordException extends RuntimeException {

	public UserWrongPasswordException() {
		super("Wrong password");
	}
}
