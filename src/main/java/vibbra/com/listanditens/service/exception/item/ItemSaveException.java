package vibbra.com.listanditens.service.exception.item;

import javax.persistence.PersistenceException;

import vibbra.com.listanditens.entity.Item;

@SuppressWarnings("serial")
public class ItemSaveException extends PersistenceException {

	public ItemSaveException(Item item) {
		super("Not able to save " + item);
	}

}
