package vibbra.com.listanditens.service.exception.item;

import org.springframework.dao.DataIntegrityViolationException;

@SuppressWarnings("serial")
public class ItemChildOfItselfException extends DataIntegrityViolationException{

	public ItemChildOfItselfException() {
		super("Item can not be child of itself");
	}
}
