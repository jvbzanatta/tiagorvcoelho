package vibbra.com.listanditens.service.exception.list;

import javax.persistence.PersistenceException;

import vibbra.com.listanditens.entity.List;

@SuppressWarnings("serial")
public class ListSaveException extends PersistenceException {

	public ListSaveException(List list) {
		super("Not able to save " + list);
	}

}
