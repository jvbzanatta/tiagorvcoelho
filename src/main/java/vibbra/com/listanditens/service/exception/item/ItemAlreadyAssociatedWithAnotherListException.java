package vibbra.com.listanditens.service.exception.item;

import org.springframework.dao.DataIntegrityViolationException;

import vibbra.com.listanditens.entity.Item;
import vibbra.com.listanditens.entity.List;

@SuppressWarnings("serial")
public class ItemAlreadyAssociatedWithAnotherListException extends DataIntegrityViolationException{

	public ItemAlreadyAssociatedWithAnotherListException(Item item, List listOwner) {
		super(item + " is already associated with " + item.getListOwner());
	}

}
