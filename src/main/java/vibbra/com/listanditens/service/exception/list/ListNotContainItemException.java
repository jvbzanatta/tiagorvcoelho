package vibbra.com.listanditens.service.exception.list;

import org.springframework.dao.DataIntegrityViolationException;

@SuppressWarnings("serial")
public class ListNotContainItemException extends DataIntegrityViolationException{

	public ListNotContainItemException(Long itemId) {
		super("List does not contain item " + itemId);
	}

}
