package vibbra.com.listanditens.service.exception.item;

import javax.persistence.EntityNotFoundException;

@SuppressWarnings("serial")
public class ItemNotFoundException extends EntityNotFoundException{

	public ItemNotFoundException(Long id) {
		super("Item with id " + id + " was not found");
	}

}
