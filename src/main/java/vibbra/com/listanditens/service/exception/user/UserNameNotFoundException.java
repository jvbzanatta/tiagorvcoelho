package vibbra.com.listanditens.service.exception.user;

@SuppressWarnings("serial")
public class UserNameNotFoundException extends RuntimeException{

	public UserNameNotFoundException(String username) {
		super("User with name " + username + " does not exist");
	}

}
