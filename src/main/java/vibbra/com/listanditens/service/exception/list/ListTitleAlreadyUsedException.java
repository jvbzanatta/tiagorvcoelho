package vibbra.com.listanditens.service.exception.list;

import javax.persistence.PersistenceException;

@SuppressWarnings("serial")
public class ListTitleAlreadyUsedException extends PersistenceException{

	public ListTitleAlreadyUsedException(String title) {
		super("List with title " + title + " already exist");
	}

}
