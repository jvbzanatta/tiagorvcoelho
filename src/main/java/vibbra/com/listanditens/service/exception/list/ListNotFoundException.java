package vibbra.com.listanditens.service.exception.list;

import javax.persistence.EntityNotFoundException;

@SuppressWarnings("serial")
public class ListNotFoundException extends EntityNotFoundException{

	public ListNotFoundException(Long id) {
		super("List with id " + id + " was not found");
	}
}
