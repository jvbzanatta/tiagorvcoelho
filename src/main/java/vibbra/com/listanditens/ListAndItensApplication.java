package vibbra.com.listanditens;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListAndItensApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListAndItensApplication.class, args);
	}

}

