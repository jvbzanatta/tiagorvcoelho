package vibbra.com.listanditens.configuration;

import javax.crypto.SecretKey;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.jsonwebtoken.security.Keys;

@Configuration
public class JwtConfiguration {

	@Bean
	public SecretKey getJwtKey() {
		return Keys.hmacShaKeyFor("TiagoRafelVolkmannCoelhoSecretKey".getBytes());
	}
}
