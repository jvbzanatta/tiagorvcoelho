package vibbra.com.listanditens.configuration;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vibbra.com.listanditens.controller.exception.ErrorMessage;
import vibbra.com.listanditens.security.AuthenticationTokenFilter;

@Configuration
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {
 
	@Autowired
	private AuthenticationTokenFilter authenticationTokenFilter;
	
	@Autowired
	private ObjectMapper mapper;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		 http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		 http.headers().frameOptions().disable();
		 http.authorizeRequests().antMatchers("/swagger-ui.html","/login","/v2/api-docs","/webjars/**","/swagger-resources/**","/h2/**","/list","/list/*").permitAll();
		 http.authorizeRequests().anyRequest().authenticated();
		 http.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
		 http.exceptionHandling().authenticationEntryPoint((request,response,authException) -> authenticationEntryPoint(response));
	}

	private void authenticationEntryPoint(HttpServletResponse response) throws JsonProcessingException, IOException {
		response.setStatus(HttpStatus.FORBIDDEN.value());
		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(new ErrorMessage("Access Denied")));
	}

}