package vibbra.com.listanditens.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableAutoConfiguration
public class SwaggerConfig {                                    
   
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("vibbra.com.listanditens"))              
          .paths(PathSelectors.any()).build().securitySchemes(apiKey()).securityContexts(securityContext())
          .tags(new Tag("ListController", "Services to create/retrieve a list and retrieve/add/update items from a list"),
        		new Tag("LoginController", "Service to retrieve JWT token"));                                           
    }

	private List<SecurityContext> securityContext() {
		List<SecurityContext> context = new ArrayList<SecurityContext>();
		context.add(SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.ant("/list/*/item/**")).build());
		return context;
	}
	
	private List<SecurityReference> defaultAuth() {
	    List<SecurityReference> reference = new ArrayList<SecurityReference>();
	    reference.add(new SecurityReference("Authorization", new AuthorizationScope [] {
	   	 new AuthorizationScope("apiKey", "accessEverything")}));
	    return reference;
	  }

	private List<ApiKey> apiKey() {
		ApiKey key = new ApiKey("Authorization", "Authorization", "header");
		return Arrays.asList(new ApiKey[] {key});
	}
}
