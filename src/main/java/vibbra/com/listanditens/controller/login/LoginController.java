package vibbra.com.listanditens.controller.login;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;
import vibbra.com.listanditens.service.UserService;

@RestController
@RequestMapping("/login")
@Api(tags="LoginController")
public class LoginController {

	@Autowired
	private UserService service;
	
	@RequestMapping(method=RequestMethod.POST)
	@ApiOperation(value = "Service to retrieve JWT token")
	public String login(@ApiParam(value="Json object containing user and password") @RequestBody @Valid LoginRequest request, @ApiIgnore Errors errors) throws Exception {
		return service.getJwtToken(request);
	}
}
