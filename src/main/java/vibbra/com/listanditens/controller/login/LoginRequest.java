package vibbra.com.listanditens.controller.login;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class LoginRequest {

	@NotEmpty
	@ApiModelProperty(example="user", required=true)
	private String username;
	
	@NotEmpty
	@ApiModelProperty(example="password", required=true)
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
