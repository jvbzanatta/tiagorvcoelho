package vibbra.com.listanditens.controller.list;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;
import vibbra.com.listanditens.controller.list.request.SaveItemToListRequest;
import vibbra.com.listanditens.controller.list.request.SaveListRequest;
import vibbra.com.listanditens.controller.list.request.UpdateItemFromListRequest;
import vibbra.com.listanditens.controller.list.response.GetItensResponse;
import vibbra.com.listanditens.controller.list.response.GetListResponse;
import vibbra.com.listanditens.controller.list.response.SaveListResponse;
import vibbra.com.listanditens.controller.list.response.SaveUpdateItemResponse;
import vibbra.com.listanditens.entity.List;
import vibbra.com.listanditens.service.ListService;
import vibbra.com.listanditens.service.exception.item.ItemChildOfItselfException;

@RestController
@RequestMapping("/list")
@Api(tags="ListController")
public class ListController {
	
	@Autowired
	private ListService service;
	
	@ApiOperation(value = "Service to retrieve list by id")
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public GetListResponse getList(@ApiParam(value="List's id", example="1") @PathVariable Long id) {
		return new GetListResponse(service.getList(id)); 
	}
	
	@ApiOperation(value = "Service to create list")
	@RequestMapping(method = RequestMethod.POST)
	public SaveListResponse saveList(@ApiParam(value="Json object containing list's title") @RequestBody @Valid SaveListRequest saveListRequest, @ApiIgnore Errors errors) throws Exception {
		List list = service.saveList(saveListRequest);
		return new SaveListResponse(list.getTitle(), list.getId());
	}
	
	@ApiOperation(value = "Service to get itens from list by list id")
	@RequestMapping(value="/{id}/item", method=RequestMethod.GET)
	public GetItensResponse getItensFromList(@ApiParam(value="List's id", example="1") @PathVariable Long id) {
		return new GetItensResponse(service.getItensFromList(id));
	}

	@ApiOperation(value = "Service to add item to list by list id")
	@RequestMapping(value="/{id}/item", method=RequestMethod.POST)
	public SaveUpdateItemResponse saveItemToList(@ApiParam(value="List's id", example="1") @PathVariable Long id, @ApiParam(value="Json object item to be added to list") @RequestBody @Valid SaveItemToListRequest request,@ApiIgnore Errors errors) throws Exception {
		return new SaveUpdateItemResponse(service.saveItemToList(id,request));
	}
	
	@ApiOperation(value = "Service to update item from list by list id")
	@RequestMapping(value="/{id}/item/{itemId}", method=RequestMethod.PUT)
	public SaveUpdateItemResponse updateItemFromList(@ApiParam(value="List's id", example="1") @PathVariable Long id, @ApiParam(value="Item's id", example="1") @PathVariable Long itemId, @ApiParam(value="Json object item to be updated") @RequestBody @Valid UpdateItemFromListRequest request, @ApiIgnore Errors errors) throws Exception {
		if(request.getParentItem() == itemId) {
			throw new ItemChildOfItselfException();
		}
		return new SaveUpdateItemResponse(service.updateItemFromList(id,itemId,request));
	}
}
