package vibbra.com.listanditens.controller.list.request;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class SaveItemToListRequest {

	@NotEmpty
	@ApiModelProperty(example="Task 1", required=true)
	private String title;
	
	@NotEmpty
	@ApiModelProperty(example="First task to be done", required=true)
	private String description;
	
	@ApiModelProperty(example="1", required=false)
	private Long parentItem;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentItem() {
		return parentItem;
	}

	public void setParentItem(Long parentItem) {
		this.parentItem = parentItem;
	}
}
