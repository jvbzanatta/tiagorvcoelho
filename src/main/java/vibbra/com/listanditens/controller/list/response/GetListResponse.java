package vibbra.com.listanditens.controller.list.response;

import vibbra.com.listanditens.entity.List;

public class GetListResponse {

	private List list;
	
	public GetListResponse() {}

	public GetListResponse(List list) {
		this.list = list;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
}
