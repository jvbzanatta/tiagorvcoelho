package vibbra.com.listanditens.controller.list.request;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

public class SaveListRequest {

	@NotEmpty
	@ApiModelProperty(example="Tasks to be done", required=true)
	private String title;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
