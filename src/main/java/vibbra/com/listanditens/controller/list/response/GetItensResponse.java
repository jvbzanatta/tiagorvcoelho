package vibbra.com.listanditens.controller.list.response;

import java.util.Set;

import vibbra.com.listanditens.entity.Item;

public class GetItensResponse {

	private Set<Item> items;

	public GetItensResponse() {}
	
	public GetItensResponse(Set<Item> items) {
		this.items = items;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}
}
