package vibbra.com.listanditens.controller.list.response;

import vibbra.com.listanditens.entity.Item;

public class SaveUpdateItemResponse {

	private Item item;

	public SaveUpdateItemResponse() {}
	
	public SaveUpdateItemResponse(Item item) {
		this.item = item;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
}
