package vibbra.com.listanditens.controller.list.request;

import io.swagger.annotations.ApiModelProperty;

public class UpdateItemFromListRequest extends SaveItemToListRequest {

	@ApiModelProperty(example="false", required=false)
	public Boolean removeChild;

	public Boolean getRemoveChild() {
		return removeChild;
	}

	public void setRemoveChild(Boolean removeChild) {
		this.removeChild = removeChild;
	}
	
}
