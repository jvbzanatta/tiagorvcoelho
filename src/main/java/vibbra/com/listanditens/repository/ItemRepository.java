package vibbra.com.listanditens.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vibbra.com.listanditens.entity.Item;

public interface ItemRepository extends JpaRepository<Item, Long> {

	Item findByTitleAndDescription(String title, String description);

	Item findByTitleAndDescriptionAndListOwnerId(String title, String description, Long listId);

	Item findByIdAndListOwnerId(Long itemId, Long listId);

}
