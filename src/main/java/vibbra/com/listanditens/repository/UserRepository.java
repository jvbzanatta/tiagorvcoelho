package vibbra.com.listanditens.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vibbra.com.listanditens.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{

	User findByName(String name);

}
