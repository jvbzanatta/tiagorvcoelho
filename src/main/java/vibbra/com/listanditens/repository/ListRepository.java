package vibbra.com.listanditens.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vibbra.com.listanditens.entity.List;

public interface ListRepository extends JpaRepository<List, Long>{

	List findByTitle(String title);

}
