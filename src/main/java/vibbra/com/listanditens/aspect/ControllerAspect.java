package vibbra.com.listanditens.aspect;

import java.util.stream.Collectors;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

@Aspect
@Component
public class ControllerAspect {

	@Pointcut("execution( * vibbra.com.listanditens.controller..*(..))")
	public void request() {};
	
	@Before("request() && args(..,errors)")
	public void validateRequest(Errors errors) throws Exception {
		if(errors.hasErrors()) {
			throw new Exception(errors.getFieldErrors().stream().map(error -> buildErrorMessage(error)).collect(Collectors.joining("")));
		}
	}
	
	private String buildErrorMessage(FieldError error) {
		return "[Field " + error.getField() + " " + error.getDefaultMessage() + "]";
	}
}
