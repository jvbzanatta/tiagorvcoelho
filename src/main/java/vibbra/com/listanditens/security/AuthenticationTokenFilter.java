package vibbra.com.listanditens.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Jwts;
import vibbra.com.listanditens.service.UserService;

@Component
public class AuthenticationTokenFilter extends OncePerRequestFilter {

	@Autowired
	private UserService userService;
	
	@Autowired
	private SecretKey secretKey;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationTokenFilter.class);
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
		throws ServletException, IOException {
			String token = request.getHeader("Authorization");
			if (token != null) {
				try {
					String userName = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
					if(userService.findUserByName(userName) != null) {
						UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userName, null, new ArrayList<>());
						SecurityContextHolder.getContext().setAuthentication(authentication);
					}
				} catch (Exception e) {
					LOGGER.warn("Failed to validate auhtorization header", e);
				}
			}
			filterChain.doFilter(request, response);
	}

}
