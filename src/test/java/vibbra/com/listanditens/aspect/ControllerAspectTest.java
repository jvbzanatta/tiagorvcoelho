package vibbra.com.listanditens.aspect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllerAspectTest {

	@Autowired
	private ControllerAspect aspect;
	
	@Test
	public void validateRequest() throws Throwable {
		Errors errors = mock(Errors.class);
		when(errors.hasErrors()).thenReturn(true);
		List<FieldError> list = new ArrayList<FieldError>();
		list.add(new FieldError("objectName", "field", "defaultMessage"));
		list.add(new FieldError("objectName2", "field2", "defaultMessage2"));
		when(errors.getFieldErrors()).thenReturn(list);
		try{
			aspect.validateRequest(errors);
			fail("Should have failed because errors has erros");
		}catch (Exception e) {
			assertEquals(e.getMessage(), "[Field field defaultMessage][Field field2 defaultMessage2]");
		}
	}
}
