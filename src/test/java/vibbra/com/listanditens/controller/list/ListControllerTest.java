package vibbra.com.listanditens.controller.list;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Set;

import javax.crypto.SecretKey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vibbra.com.listanditens.aspect.ControllerAspect;
import vibbra.com.listanditens.controller.list.request.SaveItemToListRequest;
import vibbra.com.listanditens.controller.list.request.SaveListRequest;
import vibbra.com.listanditens.controller.list.response.GetItensResponse;
import vibbra.com.listanditens.controller.list.response.GetListResponse;
import vibbra.com.listanditens.controller.list.response.SaveListResponse;
import vibbra.com.listanditens.controller.list.response.SaveUpdateItemResponse;
import vibbra.com.listanditens.entity.Item;
import vibbra.com.listanditens.entity.List;
import vibbra.com.listanditens.service.ItemService;
import vibbra.com.listanditens.service.ListService;
import vibbra.com.listanditens.service.UserService;
import vibbra.com.listanditens.service.exception.item.ItemAlreadyAssociatedWithAnotherListException;
import vibbra.com.listanditens.service.exception.item.ItemChildOfItselfException;
import vibbra.com.listanditens.service.exception.item.ItemNotFoundException;
import vibbra.com.listanditens.service.exception.item.ItemSaveException;
import vibbra.com.listanditens.service.exception.list.ListNotContainItemException;
import vibbra.com.listanditens.service.exception.list.ListNotFoundException;
import vibbra.com.listanditens.service.exception.list.ListSaveException;
import vibbra.com.listanditens.service.exception.list.ListTitleAlreadyUsedException;

@WithMockUser
@EnableAspectJAutoProxy
@RunWith(SpringRunner.class)
@WebMvcTest(controllers=ListController.class)
@Import(ControllerAspect.class)
public class ListControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ListService listService;
	
	@Autowired
	private ObjectMapper mapper;
	
	@MockBean
	private ItemService itemService;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private SecretKey secretKey;

	@Test
	public void getListSuccess() throws Exception {
		List result = createListObject();
		GetListResponse response = new GetListResponse(result);
		when(listService.getList(anyLong())).thenReturn(result);
		mockMvc.perform(get("/list/1")).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}
	
	private List createListObject() {
		List list = new List();
		list.setId(1L);
		list.setTitle("title");
		list.setItens(new HashSet<Item>());
		return list;
	}

	@Test
	public void getListListNotExist() throws Exception {
		ListNotFoundException exception = new ListNotFoundException(1L);
		when(listService.getList(anyLong())).thenThrow(exception);
		mockMvc.perform(get("/list/1")).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveListSuccess() throws Exception {
		List result = createListObject();
		SaveListResponse response = new SaveListResponse(result.getTitle(), result.getId());
		when(listService.saveList(any())).thenReturn(result);
		mockMvc.perform(post("/list").content(mapper.writeValueAsString(createSaveItemRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(content().json(mapper.writeValueAsString(response)));
	}

	private SaveListRequest createSaveItemRequest() {
		SaveListRequest saveListRequest = new SaveListRequest();
		saveListRequest.setTitle("title");
		return saveListRequest;
	}
	
	@Test
	public void saveListException() throws JsonProcessingException, Exception {
		List list = createListObject();
		ListSaveException exception = new ListSaveException(list);
		when(listService.saveList(any())).thenThrow(exception);
		mockMvc.perform(post("/list").content(mapper.writeValueAsString(createSaveItemRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError())
			.andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveListExceptionTitleAlreadyUsed() throws JsonProcessingException, Exception {
		ListTitleAlreadyUsedException exception = new ListTitleAlreadyUsedException("title");
		when(listService.saveList(any())).thenThrow(exception);
		mockMvc.perform(post("/list").content(mapper.writeValueAsString(createSaveItemRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError())
			.andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveListInvalidParameterMissingTitle() throws JsonProcessingException, Exception {
		mockMvc.perform(post("/list").content(mapper.writeValueAsString(new SaveListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError())
			.andExpect(jsonPath("$.message").value("[Field title must not be empty]"));
	}
	
	@Test
	public void getItensFromListEmptyList() throws JsonProcessingException, Exception {
		GetItensResponse response = new GetItensResponse(new HashSet<Item>());
		when(listService.getItensFromList(anyLong())).thenReturn(new HashSet<Item>());
		mockMvc.perform(get("/list/1/item")).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}
	
	@Test
	public void getItensFromListSuccess() throws JsonProcessingException, Exception {
		Set<Item> itens = createItensSet();
		GetItensResponse response = new GetItensResponse(itens);
		when(listService.getItensFromList(anyLong())).thenReturn(itens);
		mockMvc.perform(get("/list/1/item")).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}

	private Set<Item> createItensSet() {
		List list = createListObject();
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setDescription("description");
		item.setId(1L);
		item.setTitle("title");
		Item childItem = new Item();
		childItem.setDescription("description2");
		childItem.setId(2L);
		childItem.setTitle("title2");
		childItem.setListOwner(list);
		item.setChildItem(childItem);
		item.setListOwner(list);
		itens.add(item);
		list.setItens(itens);
		return itens;
	}
	
	@Test
	public void getItensFromListListDoesNotExist() throws JsonProcessingException, Exception {
		ListNotFoundException exception = new ListNotFoundException(1L);
		when(listService.getItensFromList(anyLong())).thenThrow(exception);
		mockMvc.perform(get("/list/1/item")).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveItemToListExceptionListNotExist() throws Exception {
		SaveItemToListRequest request = createSaveItemToListRequest();
		ListNotFoundException exception = new ListNotFoundException(1L);
		when(listService.saveItemToList(anyLong(),any())).thenThrow(exception);
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveItemToListExceptionItemAlreadyAsssociatedWithAnotherList() throws Exception {
		Set<Item> itens = createItensSet();
		Item item = itens.iterator().next();
		ItemAlreadyAssociatedWithAnotherListException exception = new ItemAlreadyAssociatedWithAnotherListException(item, item.getListOwner());
		when(listService.saveItemToList(anyLong(),any())).thenThrow(exception);
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveItemToListExceptionChildItemNotFound() throws Exception {
		ItemNotFoundException exception = new ItemNotFoundException(1L);
		when(listService.saveItemToList(anyLong(),any())).thenThrow(exception);
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveItemToListExceptionSaveItem() throws Exception {
		Set<Item> itens = createItensSet();
		Item item = itens.iterator().next();
		ItemSaveException exception = new ItemSaveException(item);
		when(listService.saveItemToList(anyLong(),any())).thenThrow(exception);
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void saveItemToListSuccess() throws Exception {
		Set<Item> itens = createItensSet();
		Item item = itens.iterator().next();
		SaveUpdateItemResponse response = new SaveUpdateItemResponse(item);
		when(listService.saveItemToList(anyLong(),any())).thenReturn(item);
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}

	private SaveItemToListRequest createSaveItemToListRequest() {
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("itemTitle");
		request.setDescription("itemDescription");
		return request;
	}
	
	@Test
	public void saveItemToListMissingItemTitle() throws Exception {
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setDescription("itemDescription");
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value("[Field title must not be empty]"));
	}
	
	@Test
	public void saveItemToListMissingItemDescription() throws Exception {
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("itemTitle");
		mockMvc.perform(post("/list/1/item").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value("[Field description must not be empty]"));
	}
	
	@Test
	public void updateItemFromListMissingItemTitle() throws JsonProcessingException, Exception {
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setDescription("itemDescription");
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value("[Field title must not be empty]"));
	}
	
	@Test
	public void updateItemFromListMissingItemDescription() throws JsonProcessingException, Exception {
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("itemTitle");
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value("[Field description must not be empty]"));
	}
	
	@Test
	public void updateItemFromListExceptionListNotExist() throws JsonProcessingException, Exception {
		ListNotFoundException exception = new ListNotFoundException(1L);
		when(listService.updateItemFromList(anyLong(),anyLong(),any())).thenThrow(exception);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void updateItemToListExceptionItemAlreadyAsssociatedWithAnotherList() throws Exception {
		Set<Item> itens = createItensSet();
		Item item = itens.iterator().next();
		ItemAlreadyAssociatedWithAnotherListException exception = new ItemAlreadyAssociatedWithAnotherListException(item, item.getListOwner());
		when(listService.updateItemFromList(anyLong(),anyLong(),any())).thenThrow(exception);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
		
	@Test
	public void updateItemFromListExceptionListNotContainItem() throws Exception {
		ListNotContainItemException exception = new ListNotContainItemException(1L);
		when(listService.updateItemFromList(anyLong(), anyLong(), any())).thenThrow(exception);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void updateItemFromListExceptionSaveItem() throws Exception {
		Set<Item> itens = createItensSet();
		Item item = itens.iterator().next();
		ItemSaveException exception = new ItemSaveException(item);
		when(listService.updateItemFromList(anyLong(), anyLong(), any())).thenThrow(exception);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void updateItemFromListSuccess() throws Exception {
		Set<Item> itens = createItensSet();
		Item item = itens.iterator().next();
		SaveUpdateItemResponse response = new SaveUpdateItemResponse(item);
		when(listService.updateItemFromList(anyLong(), anyLong(), any())).thenReturn(item);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(response)));
	}
	
	@Test
	public void updateItemFromListExceptionChildItemNotFound() throws Exception {
		ItemNotFoundException exception = new ItemNotFoundException(1L);
		when(listService.updateItemFromList(anyLong(),anyLong(),any())).thenThrow(exception);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(createSaveItemToListRequest())).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(exception.getMessage()));
	}
	
	@Test
	public void updateItemFromListExceptionChildItemAndItemIdAreEqual() throws Exception {
		SaveItemToListRequest request = createSaveItemToListRequest();
		request.setParentItem(1L);
		mockMvc.perform(put("/list/1/item/1").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.message").value(new ItemChildOfItselfException().getMessage()));
	}
}
