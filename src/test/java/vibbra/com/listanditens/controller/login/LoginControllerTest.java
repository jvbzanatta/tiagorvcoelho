package vibbra.com.listanditens.controller.login;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.crypto.SecretKey;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import vibbra.com.listanditens.aspect.ControllerAspect;
import vibbra.com.listanditens.service.UserService;
import vibbra.com.listanditens.service.exception.user.UserNameNotFoundException;
import vibbra.com.listanditens.service.exception.user.UserWrongPasswordException;

@EnableAspectJAutoProxy
@RunWith(SpringRunner.class)
@WebMvcTest(controllers=LoginController.class)
@Import(ControllerAspect.class)
public class LoginControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private UserService service;
	
	@MockBean
	private SecretKey secretkey;
	
	@Test
	public void loginSuccess() throws Exception {
		LoginRequest request = createLoginRequest();
		when(service.getJwtToken(any())).thenReturn("expectedContent");
		mockMvc.perform(post("/login").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(content().string("expectedContent")).andExpect(status().isOk());
	}
	
	private LoginRequest createLoginRequest() {
		LoginRequest request = new LoginRequest();
		request.setUsername("username");
		request.setPassword("password");
		return request;
	}
	
	@Test
	public void loginInvalidUser() throws Exception{
		LoginRequest request = createLoginRequest();
		UserNameNotFoundException exception = new UserNameNotFoundException(request.getUsername());
		when(service.getJwtToken(any())).thenThrow(exception);
		mockMvc.perform(post("/login").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.message").value(exception.getMessage())).andExpect(status().is5xxServerError());
	}
	
	@Test
	public void loginInvalidPassword() throws Exception{
		LoginRequest request = createLoginRequest();
		UserWrongPasswordException exception = new UserWrongPasswordException();
		when(service.getJwtToken(any())).thenThrow(exception);
		mockMvc.perform(post("/login").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.message").value(exception.getMessage())).andExpect(status().is5xxServerError());
	}
	
	@Test
	public void loginInvalidRequestEmptyUser() throws Exception{
		LoginRequest request = new LoginRequest();
		request.setPassword("password");
		String errorMessage = "[Field username must not be empty]";
		mockMvc.perform(post("/login").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.message").value(errorMessage)).andExpect(status().is5xxServerError());
	}
	
	@Test
	public void loginInvalidRequestEmptyPassword() throws Exception{
		LoginRequest request = new LoginRequest();
		request.setUsername("username");
		String errorMessage = "[Field password must not be empty]";
		mockMvc.perform(post("/login").content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.message").value(errorMessage)).andExpect(status().is5xxServerError());
	}
	
}
