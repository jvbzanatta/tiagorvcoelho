package vibbra.com.listanditens.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import vibbra.com.listanditens.configuration.JwtConfiguration;
import vibbra.com.listanditens.controller.login.LoginRequest;
import vibbra.com.listanditens.service.exception.user.UserNameNotFoundException;
import vibbra.com.listanditens.service.exception.user.UserWrongPasswordException;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({UserService.class, JwtConfiguration.class})
public class UserServiceTest {

	@Autowired
	private UserService service;
	
	@Test
	public void getJwtTokenSuccess() throws Exception {
		LoginRequest request = new LoginRequest();
		request.setUsername("user");
		request.setPassword("password");
		String result = service.getJwtToken(request);
		assertTrue(result != null && !result.isEmpty());
	}

	@Test
	public void getJwtTokenInvalidUser() throws Exception {
		LoginRequest request = new LoginRequest();
		request.setUsername("user2");
		request.setPassword("password");
		try{
			service.getJwtToken(request);
			fail("Should fail because user does not exist");
		}catch (UserNameNotFoundException e) {}
	}
	
	@Test
	public void getJwtTokenInvalidPassword() throws Exception {
		LoginRequest request = new LoginRequest();
		request.setUsername("user");
		request.setPassword("password2");
		try{
			service.getJwtToken(request);
			fail("Should fail because password is wrong");
		}catch (UserWrongPasswordException e) {}
	}
	
	@Test
	public void findUserByNameSuccess() {
		assertNotNull(service.findUserByName("user"));
	}
	
	@Test
	public void findUserByNameUserNotExist() {
		assertNull(service.findUserByName("user2"));
	} 
}
