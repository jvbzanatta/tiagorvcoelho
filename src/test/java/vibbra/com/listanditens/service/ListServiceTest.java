package vibbra.com.listanditens.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import vibbra.com.listanditens.controller.list.request.SaveItemToListRequest;
import vibbra.com.listanditens.controller.list.request.SaveListRequest;
import vibbra.com.listanditens.controller.list.request.UpdateItemFromListRequest;
import vibbra.com.listanditens.entity.Item;
import vibbra.com.listanditens.entity.List;
import vibbra.com.listanditens.service.exception.item.ItemAlreadyAssociatedWithAnotherListException;
import vibbra.com.listanditens.service.exception.item.ItemNotFoundException;
import vibbra.com.listanditens.service.exception.list.ListNotContainItemException;
import vibbra.com.listanditens.service.exception.list.ListNotFoundException;
import vibbra.com.listanditens.service.exception.list.ListTitleAlreadyUsedException;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql=true)
@Import({ListService.class,ItemService.class})
public class ListServiceTest {

	@Autowired
	private ListService listService;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	public void getListSuccess() {
		List list = createList();
		Long id = entityManager.persistAndFlush(list).getId();
		assertNotNull(listService.getList(id));
	}

	private List createList() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		Item childItem = new Item();
		childItem.setDescription("description2");
		childItem.setListOwner(list);
		childItem.setTitle("title2");
		item.setChildItem(childItem);
		item.setListOwner(list);
		itens.add(item);
		itens.add(childItem);
		list.setItens(itens);
		return list;
	}
	
	@Test
	public void getListException() {
		try {
			listService.getList(10L);
			fail("Should have thrown an exception");
		} catch (ListNotFoundException e) {}
	}
	
	@Test
	public void saveListSuccess() {
		SaveListRequest request = new SaveListRequest();
		request.setTitle("title");
		assertNotNull(listService.saveList(request));
	}
	
	@Test
	public void saveListFailListWithSameTitle() {
		List list = new List();
		list.setTitle("title");
		list.setItens(new HashSet<Item>());
		entityManager.persistAndFlush(list);
		
		SaveListRequest request = new SaveListRequest();
		request.setTitle("title");
		try {
			listService.saveList(request);
			fail("Should fail becuase there is already a list with same title");
		} catch (ListTitleAlreadyUsedException e) {}
	}
	
	@Test
	public void testGetItensFromListExceptionListNotExist() {
		try{
			listService.getItensFromList(1L);
			fail("Should fail becaus list do not exist");
		}catch (ListNotFoundException e) {}
	}
	
	@Test
	public void testGetItensFromListSuccess() {
		List list = createList();
		Long id = entityManager.persistAndFlush(list).getId();
		Set<Item> result = listService.getItensFromList(id);
		assertTrue(result != null && result.size() == 2);
	} 
	
	@Test
	public void saveItemToListListNotExist() {
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("itemTitle");
		request.setDescription("itemDescription");
		try{
			listService.saveItemToList(1L, request);
			fail("Should fail because list not exist");
		}catch (ListNotFoundException e) {}
	}
	
	@Test
	public void saveItemToListExceptionItemAlreadyExist() {
		List list = createList();
		entityManager.persistAndFlush(list).getId();
		
		List list2 = new List();
		list2.setTitle("title2");
		list2.setItens(new HashSet<Item>());
		Long id = entityManager.persistAndGetId(list2,Long.class);
		
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("title");
		request.setDescription("description");
		try {
			listService.saveItemToList(id, request);
			fail("Should fail because item with title and description is already associated with another list");
		}catch (ItemAlreadyAssociatedWithAnotherListException e) {}
	}
	
	@Test
	public void saveItemToListSameItemTwice() {
		List list = new List();
		list.setTitle("title");
		list.setItens(new HashSet<Item>());
		Long id = entityManager.persistAndFlush(list).getId();
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("title");
		request.setDescription("description");
		assertNotNull(listService.saveItemToList(id, request));
		assertNotNull(listService.saveItemToList(id, request));
		assertTrue(listService.getList(id).getItens().size() == 1);
	}
	
	@Test
	public void saveItemToListExceptionParentItemNotExist() {
		List list = new List();
		list.setTitle("title");
		list.setItens(new HashSet<Item>());
		Long id = entityManager.persistAndFlush(list).getId();
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("title");
		request.setDescription("description");
		request.setParentItem(1L);
		
		try {
			listService.saveItemToList(id, request);
			fail("should fail because item do nto exist");
		}catch (ItemNotFoundException e) {}
	}
	
	@Test
	public void saveItemToListSucess() {
		List list = new List();
		list.setTitle("title");
		list.setItens(new HashSet<Item>());
		Long id = entityManager.persistAndFlush(list).getId();
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("title");
		request.setDescription("description");
		assertNotNull(listService.saveItemToList(id, request));
	}
	
	@Test
	public void saveItemToListItemWithParentItem() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		item.setListOwner(list);
		itens.add(item);
		list.setItens(itens);
		List listSaved = entityManager.persistAndFlush(list);
		SaveItemToListRequest request = new SaveItemToListRequest();
		request.setTitle("title2");
		request.setDescription("description2");
		request.setParentItem(listSaved.getItens().iterator().next().getId());
		assertNotNull(listService.saveItemToList(listSaved.getId(), request));
		Set<Item> itensSaved = listService.getList(listSaved.getId()).getItens();
		assertTrue(itensSaved.size() == 1);
		assertNotNull(itensSaved.iterator().next().getChildItem());
	}
	
	@Test
	public void updateItemFromListExceptionListNotFound() {
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("title");
		request.setDescription("description");
		
		try {
			listService.updateItemFromList(1L, 1L, request);
			fail("Should fail because list does not exist");
		}catch (ListNotFoundException e) {}
	}
	
	
	@Test
	public void updateItemToListExceptionItemAlreadyExist() {
		List list = createList();
		entityManager.persistAndFlush(list).getId();
		
		List list2 = new List();
		list2.setTitle("title2");
		list2.setItens(new HashSet<Item>());
		Long id = entityManager.persistAndGetId(list2,Long.class);
		
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("title");
		request.setDescription("description");
		try {
			listService.updateItemFromList(id, 1L,request);
			fail("Should fail because item with title and description is already associated with another list");
		}catch (ItemAlreadyAssociatedWithAnotherListException e) {}
	}
	
	@Test
	public void updateItemFromListExceptionItemNotFound() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		item.setListOwner(list);
		itens.add(item);
		list.setItens(itens);
		Long listId = entityManager.persistAndGetId(list,Long.class);
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("title2");
		request.setDescription("description2");
		try {
			listService.updateItemFromList(listId, 1L, request);
		}catch (ListNotContainItemException e) {}
	}
	
	@Test
	public void updateItemFromListSuccess() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		item.setListOwner(list);
		itens.add(item);
		list.setItens(itens);
		List listSaved = entityManager.persistAndFlush(list);
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("title2");
		request.setDescription("description2");
		Long listId = listSaved.getId();
		listService.updateItemFromList(listId, listSaved.getItens().iterator().next().getId(), request);	
		Set<Item> itensAfterUpdate = listService.getList(listId).getItens();
		assertTrue(itensAfterUpdate.size() == 1);
		Item itemAfterUpdate = itensAfterUpdate.iterator().next();
		assertEquals(itemAfterUpdate.getTitle(), "title2");
		assertEquals(itemAfterUpdate.getDescription(), "description2");
	}
	
	@Test
	public void updateItemFromListSuccessWithChild() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		item.setListOwner(list);
		itens.add(item);
		Item item2 = new Item();
		item2.setTitle("title2");
		item2.setDescription("description2");
		item2.setListOwner(list);
		itens.add(item2);
		list.setItens(itens);
		List listSaved = entityManager.persistAndFlush(list);
		Iterator<Item> iterator = listSaved.getItens().iterator();
		Long idFirstItem = iterator.next().getId();
		Long idSecondItem = iterator.next().getId();
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("itemTitle");
		request.setDescription("itemDescription");
		request.setParentItem(idSecondItem);
		assertNotNull(listService.updateItemFromList(listSaved.getId(), idFirstItem, request));
	}
	
	@Test
	public void updateItemFromListExceptionParentItemNotExist() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		item.setListOwner(list);
		itens.add(item);
		list.setItens(itens);
		List savedList = entityManager.persistAndFlush(list);
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("title2");
		request.setDescription("description2");
		request.setParentItem(1L);
		
		try {
			listService.updateItemFromList(savedList.getId(), savedList.getItens().iterator().next().getId(), request);
			fail("should fail because item do not exist");
		}catch (ItemNotFoundException e) {}
	}
	
	@Test
	public void updateItemFromListSuccessRemoveChild() {
		List list = new List();
		list.setTitle("title");
		Set<Item> itens = new HashSet<Item>();
		Item item = new Item();
		item.setTitle("title");
		item.setDescription("description");
		item.setListOwner(list);
		itens.add(item);
		Item item2 = new Item();
		item2.setTitle("title2");
		item2.setDescription("description2");
		item2.setListOwner(list);
		itens.add(item2);
		list.setItens(itens);
		List listSaved = entityManager.persistAndFlush(list);
		Iterator<Item> iterator = listSaved.getItens().iterator();
		Long idFirstItem = iterator.next().getId();
		Long idSecondItem = iterator.next().getId();
		UpdateItemFromListRequest request = new UpdateItemFromListRequest();
		request.setTitle("itemTitle");
		request.setDescription("itemDescription");
		request.setParentItem(idSecondItem);
		assertNotNull(listService.updateItemFromList(listSaved.getId(), idFirstItem, request));
		request.setTitle("itemTitle2");
		request.setDescription("itemDescription2");
		request.setParentItem(null);
		request.setRemoveChild(true);
		assertNull(listService.updateItemFromList(listSaved.getId(), idFirstItem, request).getChildItem());
	}
	
}
