package vibbra.com.listanditens.security;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import vibbra.com.listanditens.controller.login.LoginRequest;
import vibbra.com.listanditens.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthenticationTokenFilterTest {

	@Autowired
	private AuthenticationTokenFilter filter;
	
	@Autowired
	private UserService userService;
	
	@Test
	public void doFilterInternalAuhtenticationSuccess() throws Exception {
		LoginRequest request = new LoginRequest();
		request.setUsername("user");
		request.setPassword("password");
		String jwt = userService.getJwtToken(request);
		HttpServletRequest httpRequest = mock(HttpServletRequest.class);
		HttpServletResponse httpResponse = mock(HttpServletResponse.class);
		FilterChain filterChain = mock(FilterChain.class);
		when(httpRequest.getHeader("Authorization")).thenReturn(jwt);
		filter.doFilterInternal(httpRequest, httpResponse, filterChain);
		assertNotNull(SecurityContextHolder.getContext().getAuthentication());
	}
	
	@Test
	public void doFilterInternalAuhtenticationFailInvalidJwtToken() throws Exception {
		HttpServletRequest httpRequest = mock(HttpServletRequest.class);
		HttpServletResponse httpResponse = mock(HttpServletResponse.class);
		FilterChain filterChain = mock(FilterChain.class);
		when(httpRequest.getHeader("Authorization")).thenReturn("jwt");
		filter.doFilterInternal(httpRequest, httpResponse, filterChain);
		assertNull(SecurityContextHolder.getContext().getAuthentication());
	}
	
	@Test
	public void doFilterInternalAuhtenticationFailEmptyHeader() throws Exception {
		HttpServletRequest httpRequest = mock(HttpServletRequest.class);
		HttpServletResponse httpResponse = mock(HttpServletResponse.class);
		FilterChain filterChain = mock(FilterChain.class);
		filter.doFilterInternal(httpRequest, httpResponse, filterChain);
		assertNull(SecurityContextHolder.getContext().getAuthentication());
	}
	
}
